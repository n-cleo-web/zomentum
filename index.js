var navbar = document.getElementById('navbar');
var home = document.getElementById('home');
var features = document.getElementById('features')
var services = document.getElementById('services')
var footer = document.getElementById('footer')
var ano = new Date().getFullYear()
navbar.innerHTML = `<a href="#home"><img src="Images/logo.svg" alt="logo do site"></a>
<ul>
    <li class="links"><a href="#features">Features</a></li>
    <li class="links"><a href="#services">Services</a></li>
    <li class="links"><a href="#footer">Contact</a></li>
    <li class="links">
        <a href="#">
            <button>Get a demo</button>
        </a>
    </li>
</ul>
<img src="Images/hamburguer.svg" alt="" id="hamburger">`;
home.innerHTML = `<div id="home-cima">
<h2>Find the right partners to fuel your business growth</h2>
<p>Join a vibrant community of MSPs to forge long-lasting relationships with partners that help you
     create excellent customer experiences</p>
</div>
<div id="home-baixo">
<img src="./Images/Home-left.svg" alt="pessoas realizando negócios">
<button>Register today</button>
<img src="./Images/Home-right.svg" alt="stonks">
</div>`
features.innerHTML = `<h2>List. Sell. Grow</h2>
<div id="container-features">
    <div>
        <img src="./Images/icon1.svg" alt="">
        <p>Be a part of the only  All-In-One platform for IT</p>
    </div>
    <div>
        <img src="./Images/icon2.svg" alt="">
        <p>Come closer to finding your next customer</p>
    </div>
    <div>
        <img src="./Images/icon3.svg" alt="">
        <p>Grow your business with a single click</p>
    </div>
</div>`
services.innerHTML = `<div id="services1">
<img src="./Images/services-mobile.svg" alt="" class="services-img">
<div class="services-text">
    <h2>Your channel program on steroids</h2>
    <p>Tired of finding MSPs that can help you boost your channel sales?  
    You’re in luck, because they’re all here.</p>
</div>
</div>
<div id="services2" class="services-container">
<img src="./Images/services2-mobile.svg" alt="" class="services-img">
<div class="services-text">
    <h2>Become the vendor everyone wants to buy from</h2>
    <p>Gain brand authority and visibility with the help of the largest IT service ecosystem. 
        Find customers who are looking for your solution right now!</p>
    <a href="#">Signup now &nbsp;<img src="./Images/setinha.svg" alt=""></a>
</div>
</div>
<div id="services3" class="services-container">
<img src="./Images/services3-mobile.svg" alt="" class="services-img">
<div class="services-text">
    <h2>It’s like having Your best
        Salesman on autopilot</h2>
    <p>Share collaterals and documents that are automatically branded. Leverage a rich partner
         network that sells your solution exactly the way your best salesman would.</p>
    <a href="#">Signup now &nbsp;<img src="./Images/setinha.svg" alt=""></a>
</div>
</div>
<div id="services4" class="services-container">
<img src="./Images/services4-mobile.svg" alt="" class="services-img">
<div class="services-text">
    <h2>Track your channel performance</h2>
    <p>Monitor the health and revenue of your channel program and streamline your campaign to win
         more customers within Zomentum.</p>
    <a href="#">Signup now &nbsp;<img src="./Images/setinha.svg" alt=""></a>
</div>
</div>`
footer.innerHTML = `<div id="container-footer">
<div class="footer-lados">
    <ul>
        <h3>Product</h3>
        <li><a href="#">Features</a></li>
        <li><a href="#">Pricing</a></li>
        <li><a href="#">Integrations</a></li>
        <li><a href="#">Product</a></li>
    </ul>
    <ul>
        <h3>Company</h3>
        <li><a href="#">About us</a></li>
        <li><a href="#">Contact us</a></li>
        <li><a href="#">Submit a ticket</a></li>
        <li><a href="#">Privacy policy</a></li>
        <li><a href="#">Terms & conditions</a></li>
    </ul>
</div>
<div class="footer-lados">
    <ul>
        <h3>Guides</h3>
        <li><a href="#">MSP</a></li>
        <li><a href="#">MSP Sales</a></li>
    </ul>
    <ul>
        <h3>Contact Us</h3>
        <li><a href="#">Adress</a></li>
    </ul>
</div>
</div>
<p id="copy">&copy; 2020 Pactora Inc. All rights reserved.</p>`

var copyright = document.getElementById('copy')
var date = '&copy; ' + ano + ' Pactora Inc. All rights reserved.'
const hamburger = document.getElementById('hamburger')
const links = document.getElementsByClassName('links')

copyright.innerHTML = date

function openNav(e) {
    if (e.type == 'touchstart') {
        e.preventDefault();
    }

    navbar.classList.toggle('active');
    if (hamburger.src.match('Images/hamburguer.svg')) {
        hamburger.src = 'Images/close.svg'
    } else {
        hamburger.src = 'Images/hamburguer.svg'
    }
}

hamburger.addEventListener('click', openNav);
hamburger.addEventListener('touchstart', openNav)
Array.from(links).forEach(function(links) {
    links.addEventListener('click', openNav)
})